const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const uuid = require("uuid");
const axios = require("axios");

const db = require("../models");

class userControllers {
  static async register(req, res) {
    try {
      const salt = await bcrypt.genSalt(10);
      const hashPassword = await bcrypt.hash(req.body.password, salt);
      const inputAuth = {
        id: uuid.v4(),
        username: req.body.username,
        fullname: req.body.fullname,
        email: req.body.email,
        password: hashPassword,
      };

      await db.Users.create(inputAuth);
      res.status(201).json({
        status: "Created Success",
        message: "Register Success"
      });
    } catch (error) {
      switch (error.errors[0].path) {
        case "username":
          res.status(400).json({
            status: "Register failed",
            message: "Your username has been registered",
          });
          break;

        case "email":
          res.status(400).json({
            status: "Register failed",
            message: "Your email has been registered",
          });
          break;

        default:
          res.status(500).json(error);
          break;
      }
    }
  }

  static async login(req,res) {
    try {
        const user = await db.Users.findOne({
            where:{
                username: req.body.username
            },
            raw:true
        })
        !user && res.status(404).json({
            status: "login failed",
            message:`Users with username ${req.body.username} not registered`
        });
        const isPasswordMatch = await bcrypt.compare(req.body.password, user.password);
        
        const accesToken = jwt.sign({
            id: user.id,
            role:  user.role
        },
        process.env.SECRET_KEY,
        {expiresIn: "1d"}
        )
    
        const{ password, ...others} = user;

        isPasswordMatch == true ? res.status(200).json({...others, accesToken}) : res.status(401).json({
            status: "login failed",
            message: "Wrong password"
        }); 

    } catch (error) {
        res.status(500).json(error)
    }
    
}
}

module.exports = userControllers;
