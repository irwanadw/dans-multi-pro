const axios = require("axios");
const url = require("url");
const _ = require("lodash");
const db = require("../models");

class PersonelControllers {
  static async getListJob(req, res) {
    try {
      let payload = {
        description: req.params.description,
        location: req.params.location,
        full_time: req.params.full_time || true,
        page: req.params.page || 1,
        limit: req.params.limit || 10,
      };

      if (!req.params.description) {
        delete payload.description;
      }
      if (!req.params.location) {
        delete payload.location;
      }
      const params = new url.URLSearchParams(payload);

      const result = await axios.get(
        `http://dev3.dansmultipro.co.id/api/recruitment/positions.json?${params}`
      );

      return res.status(200).json({
        status: "success",
        data: result.data,
      });
    } catch (error) {
      return res.status(500).json({
        status: "error",
        message: "error",
      });
    }
  }

  static async getDetailJob(req, res) {
    try {
      const jobId = req.params.ID;
      console.log(jobId, req)
      const result = await axios.get(
        `http://dev3.dansmultipro.co.id/api/recruitment/positions/${jobId}`
      );
      !result.data || _.isEmpty(result.data)
        ? res.status(404).json({
            status: "not found",
            message: `data with id = ${jobId} not found`,
          })
        : res.status(200).json({
            status: "success",
            data: result.data,
          });
    } catch (error) {
      return res.status(500).json({
        status: "error",
        message: error.message,
      });
    }
  }
}

module.exports = PersonelControllers;
