# dans multi pro
this is a task for recruitmen process at dans multi pro
# tech stack 
1. NodeJS
2. Express
3. Postgresql
4. Sequelize
5. Axios
6. JWT
7. Bcrypt

## Getting started
npm install

## Step to make API Request
1. Login, If not yet registered, you should register.
2. Request need auth, after login, you get a token
3. That token should be assign on header request with key and value 
    key : token
    value: Bearer <"your token">
4. enjoy 