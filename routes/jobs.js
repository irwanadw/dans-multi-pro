const express = require("express");
const router = express.Router();
const {
  verifyToken,
  verifyTokenAndAuthorization,
} = require("../middlewares/tokenAuth");
const jobControllers = require("../controllers/jobControllers");


router.get("/job/list",verifyToken, jobControllers.getListJob)
router.get("/job/detail/:ID",verifyToken, jobControllers.getDetailJob);

module.exports = router;
