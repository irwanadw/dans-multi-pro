const express = require("express");
const router = express.Router();

const usersRoute = require("./users");
const jobsRoute = require("./jobs");

router.use("/api", usersRoute);
router.use("/api", jobsRoute);

module.exports = router